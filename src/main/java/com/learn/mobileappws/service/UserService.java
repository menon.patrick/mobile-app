package com.learn.mobileappws.service;

import com.learn.mobileappws.dao.UserDao;


/**
 * @author vkongara
 */
public interface UserService {
     UserDao createUser(UserDao user);
}
