package com.learn.mobileappws.service.impl;

import com.learn.mobileappws.dao.UserDao;
import com.learn.mobileappws.io.entity.UserEntity;
import com.learn.mobileappws.io.repository.UserRepository;
import com.learn.mobileappws.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author vkongara
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;


    @Override
    public UserDao createUser(UserDao user) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(user, userEntity);

        userEntity.setEncryptedPassword("test");
        userEntity.setUserId("testUserId");
        userEntity.setEmail("jay@email.com");

        UserEntity storedUserDetails = userRepository.save(userEntity);

        UserDao returnValue = new UserDao();
        BeanUtils.copyProperties(storedUserDetails, returnValue);

        return returnValue;
    }
}
